{ fetchFromBittorrent
, runCommand
, unzip
, dosbox-x
, xvfb-run
, x11vnc
, tesseract
, expect
, vncdo
, writeScript
, writeShellScript
}:
let
  dosboxConf = ./dosbox.conf;
  win30 = fetchFromBittorrent {
    url = "https://archive.org/download/windows-3.0-720-kb-disks/windows-3.0-720-kb-disks_archive.torrent";
    hash = "sha256-PJ+qM0lFSYy+DC08myJGrtDQGZOQHltc0JRjV+niNXA=";
  };
  msdos622 = fetchFromBittorrent {
    url = "https://archive.org/download/MS_DOS_6.22_MICROSOFT/MS_DOS_6.22_MICROSOFT_archive.torrent";
    hash = "sha256-B88pYYF9TzVscXqBwql2vSPyp2Yf2pxJ75ywFjUn1RY=";
  };

  tesseractScript = writeShellScript "tesseractScript" ''
    cd $(mktemp -d)
    TEXT=""
    while true
    do
      sleep 1
      ${vncdo}/bin/vncdo -s 127.0.0.1::5900 capture cap.png
      NEW_TEXT="$(${tesseract}/bin/tesseract cap.png stdout 2>/dev/null)"
      if [ "$TEXT" != "$NEW_TEXT" ]; then
        echo "$NEW_TEXT"
        TEXT="$NEW_TEXT"
      fi
    done
  '';

  expectScript = let
    vncdoWrapper = writeScript "vncdoWrapper" ''
      ${vncdo}/bin/vncdo --force-caps -s 127.0.0.1::5900 "$@"
    '';
  in writeScript "expect.sh"
  ''
    #!${expect}/bin/expect -f
    set debug 5
    set timeout -1
    spawn ${tesseractScript}
    expect "To continue Setup, press ENTER."
    exec ${vncdoWrapper} move 800 0
    exec ${vncdoWrapper} key enter
    expect "unallocated disk space"
    exec ${vncdoWrapper} key enter
    expect "Setup will restart your computer"
    exec ${vncdoWrapper} key enter
    expect "The settings are correct"
    exec ${vncdoWrapper} key enter
    expect "Setup will place your MS-DOS files in the following"
    exec ${vncdoWrapper} key enter
    expect "Disk #2"
    exec ${vncdoWrapper} key f12-o pause 1 key enter
    expect "Disk #3"
    exec ${vncdoWrapper} key f12-o pause 1 key enter
    expect "Remove disks"
    exec ${vncdoWrapper} key enter
    expect "MS-DOS Setup Complete"
    exec ${vncdoWrapper} key enter
    expect "Microsoft MS-DOS 6.22 Setup"
    send_user "\n### OMG DID IT WORK???!!!! ###\n"
    exit 0
  '';

in
runCommand "msdos622.img" { buildInputs = [ unzip dosbox-x xvfb-run x11vnc ]; __impure = true; } ''
#  echo ${win30}
  echo ${msdos622}
  mkdir win
  mkdir msdos
#  unzip '${win30}/Microsoft Windows 3.0 (3.5-720K).zip' -d win
  unzip '${msdos622}/MS DOS 6.22.zip' -d msdos
#  dd if=/dev/zero of=disk.img bs=1000 count=838656
  dd if=/dev/zero of=disk.img count=8192 bs=31941

  xvfb-run -l -s ":99 -auth /tmp/xvfb.auth -ac -screen 0 800x600x24" dosbox-x -conf ${dosboxConf} || true &
  dosboxPID=$!
  DISPLAY=:99 XAUTHORITY=/tmp/xvfb.auth x11vnc -many -shared -display :99 >/dev/null 2>&1 &
  ${tesseractScript} &
  ${expectScript} &
  wait $!
  kill $dosboxPID
  mv disk.img $out
''
